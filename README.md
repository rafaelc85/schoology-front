
## Requirements

You must have the requirements below to run this project:

1. Git
2. npm
3. Angular CLI
4. Clone the back end from **git clone https://rafaelc85@bitbucket.org/rafaelc85/schoology.git**, follow the instructions on it's README file and run it. 

---

## How to Run 

1. Clone the front end repository **git clone https://rafaelc85@bitbucket.org/rafaelc85/schoology-front.git**
2. cd into **schoology-front**
3. Run **ng serve** for a dev server.
4. Navigate to **http://localhost:4200/**
5. Search an english word to auto complete

---

## General considerations and credits

1. Since it's a back end test, I made a simple application in Angular, just to show the information requested. 
2. I've got the list of words from here: https://github.com/dwyl/english-words/, then I removed the ones with less than 3 words 
3. I limited the number of results to 10
4. I didn't apply any filter or sorting on results

