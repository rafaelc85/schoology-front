import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  endpoint: string;

  constructor(private http: HttpClient) { 
    
    this.endpoint = 'http://localhost:8080/';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };

  }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }

  getWords(search): Observable<any> {
    return this.http.get(this.endpoint + '?search=' + search).pipe(
      map(this.extractData));
  }  

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); 
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

}
